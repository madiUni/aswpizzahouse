const Notification = {
            template: `<div class="notifications">
                    <link rel="stylesheet"
                      href="https://cdnjs.cloudflare.com/ajax/libs/semantic-ui/2.4.1/semantic.min.css">



                    <div id="a"
                           class="ui container">
                          <h1> </h1>
                        <h1>Notification Center</h1>

                           <div class="ui message" :class="type" v-if="!hidden">
                                         <i @click="hide" class="close icon"></i>
                                          <div class="message">
                                          <p>I seguenti prodotti sono scesi sotto la quantità minima:
                                          <li class="list-group-item" v-for="message in messages">
                                            <span>
                                              {{message.nameProd}} - la quantità da ordinare: {{message.qtyProd}}

                                            </span>
                                          </li>
                                          </div>
                                         <slot></slot>
                             </div>




                      </div>`,


          props: {
                 type: {
                   type: String,
                   default: 'info'
                 },
                 header: {
                   type: String,
                  // default: ' Look at orders! Stock has been changed.'
                 }
               },
          data() {
            return {
              hidden: false,
              messageNameProduct: '',
              messageQuantityProduct: '',
             messages: []
            }
          },

          methods: {
               hide() {
                 this.hidden = true
               },
               showMessage(){
                 var socket = io();
                         socket.on('low good', (data) => {
                             this.messages.push({
                                                nameProd: data.name,
                                                qtyProd: data.stock,
                                            });

                         });

             }
           },
           mounted() {
             this.showMessage();
          }


 }
