const router = new VueRouter({
    mode: 'history',
    routes: [
      { path: '/', component: Home },
      { path: '/404', component: NotFound },
      { path: '/ranking', component: Ranking },
      { path: '/orders', component: Orders },
      { path: '/notification', component: Notification },
      { path: '/login', component: Login },
      { path: '/stock', component: Stock },
      { path: '/salumiFormaggi', component: Stock },
      { path: '/digestive', component: Stock },
      { path: '/ortofrutta', component: Stock },
      { path: '/bibita', component: Stock },
      { path: '/farinaLievito', component: Stock },
      { path: '/user', component: User },
      { path: '*', redirect: '/404' }
    ]
  })
