var express = require('express');
var app = express();
var mongoose = require('mongoose');
var Warehouse = require('./src/models/warehouseModel');
var http = require('http').Server(app);
var io = require('socket.io')(http);

const PORT = 8080;

var bodyParser = require('body-parser');
//mongoose.connect('mongodb+srv://applicazioniweb2019:applicazioniweb2019@cluster0-f3q4q.gcp.mongodb.net/dbwarehouse', { useNewUrlParser: true, useFindAndModify: false });
mongoose.connect('mongodb://localhost/dbwarehouse', { useUnifiedTopology: true, useNewUrlParser: true, useFindAndModify: false });

//Per gestire i parametri passati nel corpo della richiesta http.
app.use(bodyParser.urlencoded({ extended: true }));
app.use(bodyParser.json());
app.use('/static', express.static(__dirname + '/public'));
//Vue.use(Notifications);
//app.use(bodyParser.notification;

var path = require('path');
global.appRoot = path.resolve(__dirname);

var routes = require('./src/routes/warehouseRoutes');
routes(app);

app.use(function(req, res) {
  res.status(404).send({url: req.originalUrl + ' not found'})
});

io.on('connection', function(socket){
  console.log('a user connected');
    socket.on('disconnect', function(){
    console.log('user disconnected');
    });
  socket.on('low good',function(data){

    io.emit('low good', data);
    console.log('server intercetta evento e lo propaga ad altri client');
  })
});

//app.
http.listen(PORT, function () {
  console.log('Node API server started on port '+ PORT);
});
